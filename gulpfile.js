var gulp = require('gulp');
var sass = require('gulp-sass');
var auto = require('gulp-autoprefixer');
var plum = require('gulp-plumber');
var rename = require('gulp-rename');
var comb = require('gulp-csscomb');
gulp.task('sass', function(){
	gulp.src('./style.scss')
		.pipe(plum())
		.pipe(comb())
		.pipe(auto({browsers: ["last 2 versions", "ie >= 8"] ,
			cascade: false
		}))
		.pipe(sass({outputStyle:"compact"}))
		.pipe(rename('./add.css'))
		.pipe(gulp.dest('./css'));
});
gulp.task('watch', function(){
	gulp.watch('./style.scss', ['sass']);
})

